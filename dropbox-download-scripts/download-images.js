var request = require('request');
var fs = require('fs');
var exec = require('child_process').execSync;

var stock = process.argv[2];
var start_folder = process.argv[3];

if(start_folder){
    start_folder = parseInt(start_folder);
}

if(!stock){
    throw 'Must provide stock';
}

var files_endpoint = 'https://api.dropboxapi.com/2/files/';
var content_endpoint = 'https://content.dropboxapi.com/2/files/';

// Token should be encrypted, but is not for demo
var auth_str = 'Bearer VWlK2caLDUAAAAAAAAAAGl3GgEP_998z1By3ZVUXaseE41gdVszQTpzBpAp8O1-w';

var image_dir = '/dropbox-download-scripts/images';

if(!fs.existsSync(image_dir)){
    fs.mkdirSync(image_dir);
}

exec('rm -rf ' + image_dir + '/*');

search();

function search(){
    var search_endpoint = files_endpoint + 'search';

    var data = {
        path: '/PowerSportsImages',
        query: stock
    };

    if(start_folder){
        data.start = start_folder;
    }

    var body = JSON.stringify(data);

    console.log('Searching ' + stock);

    request({
        url: search_endpoint,
        method: 'POST',
        body: body,
        headers: {
            'Authorization': auth_str,
            'Content-Length': body.length,
            'Content-Type': 'application/json'
        }
    }, function(err, res, body){
        if(err){
            throw 'Error searching: ' + err;
        }

        var data;
        try{
            data = JSON.parse(body);
        }catch(err){
            throw 'Error parsing search results: ' + err;
        }

        (function(){
            /*
                TODO
                - Possibly need to page, but not likely
            */
            var current_index = 0;

            list_next_folder();

            function list_next_folder(){
                if(current_index >= data.matches.length){
                    throw 'Unable to find images';
                }

                var this_index = current_index;
                ++current_index;

                var match_meta = data.matches[this_index].metadata;

                if(match_meta['.tag'] !== 'folder'){
                    console.log('Search result not folder. Listing next.');

                    list_next_folder();

                    return;
                }

                get_images(match_meta, function(images){
                    if(!images){
                        console.log('No images found. Listing next folder');

                        list_next_folder();

                        return;
                    }

                    download_images(images);
                });
            };
        })();
    });
};

function get_images(folder_meta, cb){
    var list_folder_endpoint = files_endpoint + 'list_folder';
    var path = folder_meta.path_display;

    var data = {
        path: path
    };

    var body = JSON.stringify(data);

    console.log('Listing folder and searching for images: ' + folder_meta.name);

    request({
        url: list_folder_endpoint,
        method: 'POST',
        body: body,
        headers: {
            'Authorization': auth_str,
            'Content-Type': 'application/json',
            'Content-Length': body.length
        }
    }, function(err, res, body){
        if(err){
            throw 'Error listing folder: ' + err;
        }

        var data;
        try{
            data = JSON.parse(body);
        }catch(err){
            console.log(body);

            throw 'Error parsing list data: ' + err;
        }

        var images;
        var imgs = [];
        for(var i = 0; i < data.entries.length; ++i){
            var f = data.entries[i];

            if(f['.tag'] !== 'file' || !/\.jpg/i.test(f.name)){
                console.log('Not file or jpeg, so ignoring: ' + f.name);

                continue;
            }

            console.log('Adding image: ' + f.name);

            imgs.push(f);
        }

        if(imgs.length){
            images = imgs;
        }


        cb(images);
    });
};

function download_images(images){
    // Craigslist allows a max number of 24 photos, so only grab the first 24 photos
    var these_images = (function(){
        var sort_order = 1;

        return images.sort(function(a, b){
            if(a.name > b.name) return 1 * sort_order;

            if(a.name < b.name) return -1 * sort_order;

            return 0;
        }).slice(0, 24);
    })();

    var current_index = 0;

    console.log('Downloading ' + these_images.length + ' images');

    next_image();

    function next_image(){
        if(current_index >= these_images.length){
            console.log('Finished downloading images');

            return;
        }

        var this_index = current_index;
        ++current_index;

        var image = these_images[this_index];

        console.log('Downloading ' + image.name);

        download_image(image.id, this_index, function(){
            console.log('Downloaded ' + image.name);

            next_image();
        });
    };
};

function download_image(id, index, cb){
    var download_endpoint = content_endpoint + 'download';

    var data = {
        path: id
    };

    var body = JSON.stringify(data);

    var image_path = image_dir + '/' + index + '.jpg';

    request({
        url: download_endpoint,
        method: 'POST',
        headers: {
            'Authorization': auth_str,
            'Dropbox-Api-Arg': body
        }
    })
        .pipe(fs.createWriteStream(image_path))
        .on('close', function(){
            cb();
        })
        .on('error', function(err){
            fs.unlink(image_path);

            throw 'Error downloading image: ' + id + ' - ' + err;
        });
};
