FROM ubuntu:16.04

ADD system-scripts /root/scripts
WORKDIR /root/scripts

RUN ./install

USER dropbox-download

ENTRYPOINT ["./run"]
